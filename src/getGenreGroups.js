const mongoose = require('mongoose');
const genreGroupSchema = require("./schemas/genresGroup");

mongoose.connect('mongodb://sky:sky123@ds050739.mlab.com:50739/amazon_movies_id', {useNewUrlParser: true});

//get genergroups
var genreUrlCollection = [];
//monta a query para execução
var query = genreGroupSchema.find({}).lean();
//realiza operacoes com os resultados
function getGenerGroupFiltered() {
    return new Promise((resolve, reject) => {
        query.exec(function(err,docs) {
            docs.forEach(doc => {
                genreUrlCollection.push(doc);
            });
            //inserir dentro do contexto asyncrono
            resolve(genreUrlCollection);
        });
    });
}

//funcao responsavel por gerenciar a promise getgeersgroups
async function getData() {
    var genresUrl = await getGenerGroupFiltered();
    return genresUrl;
}

module.exports = getData();

