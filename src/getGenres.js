const mongoose = require('mongoose');
const genreSchema = require("./schemas/genre");

mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});

//get getgenres
var genreUrlCollection = [];
//monta a query para execução
var query = genreSchema.find({}).lean();
//realiza operacoes com os resultados
function getGenres() {
    return new Promise((resolve, reject) => {
        query.exec(function(err,docs) {
            docs.forEach(doc => {
                genreUrlCollection.push(doc);
            });
            //inserir dentro do contexto asyncrono
            resolve(genreUrlCollection);
        });
    });
}

//funcao responsavel por gerenciar a promise getgenres
async function getData() {
    var genresUrl = await getGenres();
    console.log(genresUrl);
    return genresUrl;
}

module.exports = getData();

