const main = require('../main');
const genres = require("./getGenres");
const getGenres = require('./filterGenres');
var genreList = [];
var urlList = [];

(function schedule() {
    main.then(function() {
        console.log('Process finished, waiting 3 minutes');
        //get genres 5 minutes
        setTimeout(function() {
            console.log('Reiniciando coleta de Gêneros...');
            genres.then(function(results) {
                console.log('Recebendo lista de Gêneros (Urls)');
                //MAIN FUNCTION
                results.forEach(genre => {
                    if(genre.url != '') {
                        genreList.push(genre);
                        urlList.push("http://www.amazon.com"+genre.url);
                    }
                });
                console.log(urlList[0]);
                //executando FilterGenres a partir da lista recebida
                setTimeout(() => {
                    //iniciando scrapp de subGeneros
                    getGenres(urlList);
                    //resetando rotina
                    schedule();
                }, 1000 * 60 * 3);
            });
        }, 1000 * 60 * 1);
    }).catch(err => console.error('error in scheduler', err));
})();