var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var movieSchema = new Schema({
    status: String,
    name:  String,
    full_url: String,
    asin: String,
    genre: String,
    page: String
});

module.exports = mongoose.model('VideoIds', movieSchema);