var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var genresGroup = new Schema({
    genreId: String,
    genreName: String,
    url: String,
    subGendName: String,
    dataRange: String,
    lastUpdatedBegin: Date,
    lastUpdatedEnd: Date,
    allPages: Number,
    lastPage: Number,
    lastPageUrl: String
});

module.exports = mongoose.model('GenreGroup', genresGroup);