const to = promise => promise
	.then(data => [null, data])
	.catch(err => [err, null]);

const throwError = (errMessage, log, error, detail) => {
	if (log) console.error(errMessage);
	if (error) console.error(error);
	//Add true in last parameter to avoid closing browser and stop execution
	if (!detail) process.exit();
	throw new Error(errMessage);
};

module.exports = { to, throwError };
