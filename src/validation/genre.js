const Joi = require('joi');

const Genre = Joi.object().keys({
	name: Joi.string().required(),
	url: Joi.string().required(),
	lastUpdated: Joi.date().required(),
});

Genre.validate = function(value) {
	return Joi.validate(value, Genre, { convert: true });
};

const SubGenre = Joi.object().keys({
	genreId: Joi.string().required(),
	genreName: Joi.string().required(),
	url: Joi.string().required(),
	subGenderName: Joi.string().required(),
	dataRange: Joi.string().required(),
	lastUpdatedBegin: Joi.date().required(),
	lastUpdatedEnd: Joi.date().required(),
	allPages: Joi.date().required(),
	lastPage: Joi.date().required(),
	lastPageUrl: Joi.string().required(),
});

SubGenre.validate = function(value) {
	return Joi.validate(value, SubGenre, { convert: true });
};

module.exports = { Genre, SubGenre };
