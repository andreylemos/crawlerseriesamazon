const Puppeteer = require('puppeteer-core');
const { throwError } = require('./utils/helper');
const { Genre } = require('./validation/genre');
const fs = require('fs');
let fsExtra = require('fs-extra');
let chromeTmpDataDir = null;

var ALL_GENRES_URL;;
const JQUERY_URL = process.env.JQUERY_URL;

class Scraper {
	constructor() { }

	async startBrowser() {
		this.browser = await Puppeteer.launch({
			executablePath: process.env.BROWSER_EXEC_PATH,
			headless: false,
			/** Passe a opção devtools para executar o debug dentro do contexto do browser,
				Utilize a flag debugger; no seu código como breakpoint.
			*/
			devtools: true,
		});

		//Clear temp folder
		/* let chromeSpawnArgs = this.browser.process().spawnargs;
		for (let i = 0; i < chromeSpawnArgs.length; i++) {
			if (chromeSpawnArgs[i].indexOf("--user-data-dir=") === 0) {
				chromeTmpDataDir = chromeSpawnArgs[i].replace("--user-data-dir=", "");
			}
		}

		fs.readdirSync(chromeTmpDataDir+"/../").forEach(file => {
			var dir = file.split("-");
			var actualDir = chromeTmpDataDir.split("\\").pop();
			var path = chromeTmpDataDir+"/../";
			if(dir[0] == "puppeteer_dev_profile"){
				if(file != actualDir){
					console.log(path+file);
					fsExtra.removeSync(path+file);
				}
				
			}
		}); */
	}

	async closeBrowser() {
		await this.browser.close();
	}

	async getGenres(type=null) {
		ALL_GENRES_URL = (type == 'movies') ? process.env.URL_ALL_MOVIES : process.env.URL_ALL_SERIES;
		console.log(ALL_GENRES_URL);
		const page = await this.browser.newPage();
		await page.setViewport({ width: 360, height: 600 });
		await page.goto(ALL_GENRES_URL, { waitUntil: 'networkidle2' });
		await page.waitForSelector('#refinementList');
		await page.addScriptTag({ url: JQUERY_URL });

		let prefix = "";
		if(type == 'movies'){
			prefix = "Movies - ";
		}
		else if(type == 'tv'){
			prefix = "TV - ";
		}

		const genres = await page.evaluate((prefix) => {
			let genres = [];
			// eslint-disable-next-line no-undef
			const selector = $('#refinementList');

			let genresNames = [];
			selector.find('a').map(function(index, elem){
				// eslint-disable-next-line no-undef
				const name = prefix.toString() + $.trim($(this).attr('title'));
				genresNames.push(name);
			});
			let genresUrl = [];
			selector.find('a').map(function(){
				const BASE_URL = 'https://www.amazon.com';
				// eslint-disable-next-line no-undef
				const url = BASE_URL + $.trim($(this).attr('href'));
				genresUrl.push(url);
			});

			// const genresNames = selector.find('a').text().get();
			// const genresUrl = selector.attr('href').get();
			genres = genresNames.map((name, index) => {
				const genre = {
					name: name,
					url: genresUrl[index],
					lastUpdated: new Date().toLocaleString()
				};

				return genre;
			});
			return genres;
		},prefix);

		const validGenres = genres.map((genre, index) => {
			// const [ error, validGenre ] = Genre.validate(genre);
			const { error, value } = Genre.validate(genre);
			if (error) throwError('Um gênero foi coletado com valores inválidos: ' +
				`\nNome: ${genre.name}; Url: ${genre.url} no index: ${index}`, true);

			return value;
		});

		console.log('Resultado da validação: ', validGenres);
		page.close();
		return validGenres;
	}

	// metodo de coleta de sub-generos
	async getSubGenres(url) {
		var contentType = 'tv';
		const page = await this.browser.newPage();
		await page.setViewport({ width: 360, height: 600 });
		await page.goto(url, { waitUntil: 'networkidle2' });
		await page.addScriptTag({ url: JQUERY_URL });
		await page.waitForSelector('body');

		const actualSubGPage = await page.evaluate((contentType) => {
			var subGenreList = [];
			var subGenreDataList = [];
			var fullfilteredUrls = [];

			const selector = $('body');
			var allUrlFilters = selector.find('.a-list-item .a-declarative').toArray();

			if(contentType == 'tv'){
				let genderInsertion = {
					subGendercontentType: contentType,
					genreName: $('span.a-list-item > h4.a-size-small.a-color-base.a-text-bold').text(),
					subGenreName: $('span.a-list-item > h4.a-size-small.a-color-base.a-text-bold').text(),
					dataRange: "N/A Filtro de Data",
					url: window.location.href
				};
				fullfilteredUrls.push(genderInsertion);
			}

			// get all sug-genders to scrapp
			allUrlFilters.forEach(filter => {
				if (filter.attributes['data-s-ref-selected'].value.substr(9).search('sr_nr_p_n_feature_four_bro_') != -1) {
					let filterUrl = filter.attributes['data-s-ref-selected'].value.substr(9).slice(0, -2);
					let filterUrlNew = filterUrl.replace(/amp;/g, '');
					let name = filter.lastChild.lastChild.lastChild.lastChild.lastChild.data;
					filterUrlNew = [name, filterUrlNew.substring(0, filterUrlNew.search('&bbn'))];
					// push to sub-genres local list
					subGenreList.push(filterUrlNew);
				}
			});

			// get all data ranges from that gender
			allUrlFilters.forEach(filter => {
				if (filter.attributes['data-s-ref-selected'].value.substr(9).search('sr_nr_p_n_feature_three_br_') != -1) {
					let filterUrl = filter.attributes['data-s-ref-selected'].value.substr(9).slice(0, -2);
					let filterUrlNew = filterUrl.replace(/amp;/g, '');
					let date = filter.lastChild.lastChild.lastChild.lastChild.lastChild.data;
					filterUrlNew = [date, filterUrlNew.substring(0, filterUrlNew.search('&bbn'))];
					// push to sub-genres local list
					subGenreDataList.push(filterUrlNew);
				}
			});

			//exception for educational links
			if(subGenreList.length > 0) {
				// integrates the genre filter url with the date filter url from that gender
				var firstUrlAdd = false;
				subGenreList.forEach(subGender => {
					let actualSubGender = 'https://www.amazon.com/s/' + subGender[1];
					if(subGenreDataList.length > 0) { //also have data filters
						subGenreDataList.forEach(subGenDataFilter => {
							let fullSubGenderUlr = actualSubGender + subGenDataFilter[1].substring(subGenDataFilter[1].lastIndexOf('%2Cp'));
							let genderInsertion = {
								subGendercontentType: contentType,
								genreName: $('span.a-list-item > h4.a-size-small.a-color-base.a-text-bold').text(),
								subGenreName: subGender[0],
								dataRange: subGenDataFilter[0],
								url: fullSubGenderUlr,
							};
							fullfilteredUrls.push(genderInsertion);
						});
					} else { //no data filters (series genres)
						let genderInsertion = {
							subGendercontentType: contentType,
							genreName: $('span.a-list-item > h4.a-size-small.a-color-base.a-text-bold').text(),
							subGenreName: subGender[0],
							dataRange: "N/A Filtro de Data",
							url: actualSubGender,
						};
						fullfilteredUrls.push(genderInsertion);

						//check if first url has been add if is series url
						if(!firstUrlAdd) {
							//add first page of genre to flteredUrls
							let genderInsertion = {
								subGendercontentType: contentType,
								genreName: $('span.a-list-item > h4.a-size-small.a-color-base.a-text-bold').text(),
								subGenreName: subGender[0],
								dataRange: "N/A Filtro de Data",
								url: window.location.href
							};
							fullfilteredUrls.push(genderInsertion);
							firstUrlAdd = true; //change flag
						}
					}
				});
			} else if (subGenreDataList.length > 0) { //if educational (has no subgenres)
				if(contentType == 'movie'){
					let actualSubGender = 'https://www.amazon.com/s/s/ref=sr_nr_p_n_feature_three_br_0?fst=as%3Aoff&rh=n%3A2858778011%2Cp_n_theme_browse-bin%3A14049034011';
					//filter only by data
					subGenreDataList.forEach(subGenDataFilter => {
						let fullSubGenderUlr = actualSubGender + subGenDataFilter[1].substring(subGenDataFilter[1].lastIndexOf('%2Cp'));					
						let genderInsertion = {
							subGendercontentType: contentType,
							genreName: $('span.a-list-item > h4.a-size-small.a-color-base.a-text-bold').text(),
							subGenreName: 'Educational',
							dataRange: subGenDataFilter[0],
							url: fullSubGenderUlr,
						};
						fullfilteredUrls.push(genderInsertion);
					});
				}
			}
			return fullfilteredUrls;
		}, contentType);

		page.close();
		
		return actualSubGPage;
	}

	async scrapMovie() {
		const page = await this.browser.newPage();
		await page.setViewport({ width: 360, height: 600 });
		await page.goto(ALL_GENRES_URL, { waitUntil: 'networkidle2' });
		await page.waitForSelector('#refinementList');
		await page.addScriptTag({ url: JQUERY_URL });

		const body = await page.evaluate(() => {
			return $('body');
		});
		await page.close();
		return body;
	}
}

module.exports = Scraper;
