//serve apenas para resetar parte da tabela de gendergroups e não ser necessário recriar toda a estrutura 
//para salvar as paginas
const mongoose = require('mongoose');
const genreGroupSchema = require('./schemas/genresGroup');

mongoose.connect('mongodb://sky:sky123@ds050739.mlab.com:50739/amazon_movies_id', {useNewUrlParser: true});

genreGroupSchema.updateMany({}, { $set: {"lastUpdatedBegin" : null}}, {multi:true}, function(err, docs) {});
genreGroupSchema.updateMany({}, { $set: {"lastUpdatedEnd" : null}}, {multi:true}, function(err, docs) {});
genreGroupSchema.updateMany({}, { $set: {"allPages" : null}}, {multi:true}, function(err, docs) {});
genreGroupSchema.updateMany({}, { $set: {"lastPage" : null}}, {multi:true}, function(err, docs) {});
genreGroupSchema.updateMany({}, { $set: {"lastPageUrl" : null}}, {multi:true}, function(err, docs) {});
