//const mongoose = require('mongoose');

const { SubGenre } = require('./database/genre');
const movieSchema = require('./database/videoid');
const genreGroupSchema = SubGenre;
const puppe = require('puppeteer-core');
const cheerio = require('cheerio');
const fs = require('fs');
let fsExtra = require('fs-extra');
var sleep = require('system-sleep');
var genreGroups = getGenreGroups();
var movies = getMovies();
let chromeTmpDataDir = null;

//mongoose.connect('mongodb://sky:sky123@ds163764.mlab.com:63764/amazon_teste_carga', {useNewUrlParser: true});
//mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});

// Conexão com o banco de dados
require('./database/connection');

//### preparing log ###
var logDetail = {};
logDetail['genre'] = "";
logDetail['subgenre'] = "";
logDetail['decade'] = "";
logDetail['filter_url'] = "";
logDetail['filter_total_pages'] = "< 6";
logDetail['page'] = "1";
logDetail['page_url'] = "";
logDetail['page_status'] = "";
logDetail['total_movies_in_page'] = "";
logDetail['new_movies_in_page'] = "";
logDetail['existing_movies_in_page'] = "";
logDetail['page_movies'] = [];

var logInfo = {};
logInfo['genre'] = "";
logInfo['subgenre'] = "";
logInfo['decade'] = "";
logInfo['filter_url'] = "";

var logTotal = {};
logTotal['total_movies'] = 0;
logTotal['total_new_movies'] = 0;
logTotal['total_existing_movies'] = 0;
logTotal['total_pages'] = 0;

var logError = {};
logError['page_url'] = "";
logError['page_status'] = "";
logError['error'] = "";
//######

//### log vars ###
var movieAmount = 1;
//######

//contador filmes
var movieAmount = 0;
var moviesList = [];
var genreGroupsList = [];
var movieAsinLocalList = [];

//urlList
var urlList = [];
var needle = 0;

var adultFilter = true;

async function startBrowser() {
    this.browser = await puppe.launch({
        executablePath: "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe",
        // slowMo: Math.random() * 1,
        headless: false,
    });

    //Clear temp folder
    /* let chromeSpawnArgs = this.browser.process().spawnargs;
    for (let i = 0; i < chromeSpawnArgs.length; i++) {
        if (chromeSpawnArgs[i].indexOf("--user-data-dir=") === 0) {
            chromeTmpDataDir = chromeSpawnArgs[i].replace("--user-data-dir=", "");
        }
    }

    fs.readdirSync(chromeTmpDataDir+"/../").forEach(file => {
        var dir = file.split("-");
        var actualDir = chromeTmpDataDir.split("\\").pop();
        var path = chromeTmpDataDir+"/../";
        if(dir[0] == "puppeteer_dev_profile"){
            if(file != actualDir){
                console.log(path+file);
                fsExtra.removeSync(path+file);
            }
            
        }
    }); */
}

async function closeBrowser() {
    await this.browser.close();
}

async function startCraw(url) {
    const page = await this.browser.newPage();
    const response = await page.goto(url);

    if (response.headers().status != 200) {
        console.log('Status Code = %s', response.headers().status);
    }

    const html = await page.evaluate(() => document.documentElement.outerHTML);
    var $ = cheerio.load(html);

    //### log ###
    logDetail['page_status'] = response.headers().status;
    //######

    scrappPage($, checkNext);
    await page.close();

}

async function openLink(link){
    var pageAdult = await this.browser.newPage();
    await pageAdult.goto(link);
    await pageAdult.close();
}

//get the movies values
/* var c = new Crawler({
    maxConnections : 3,
    // This will be called for each crawled page
    callback : function (error, res, done) {
        if(error){
            console.log(error);
            //### log ###
            logError['page_url'] = res.request.uri;
            logError['page_status'] = res.statusCode;
            logError['error'] = error;

            fs.appendFile('CrawlerLog.txt', "\n\n//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"+JSON.stringify(logError)+",\n//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n", (err) => {
                if (err) throw err;
                console.log('O arquivo error foi salvo!');
            })

            logError['page_url'] = "";
            logError['page_status'] = "";
            logError['error'] = "";
            //######
        }else{
            var $ = res.$;

            //### log ###
            logDetail['page_status'] = res.statusCode;
            //######

            scrappPage($, checkNext);
            done();
        }
    }
}); */

//inside page movie data list scrapp function
function scrappPage(dom, callback) {
    var asinList = [];
    var nextPage = false;

    //### log vars ###
    var qtdNew = 0;
    var qtdExist = 0;
    var qtdPage = 0;
    var moviesInfo = {};
    //######

    //Adult filter

    if(adultFilter){
        var adultLink = dom("h2.a-size-medium.a-spacing-base.a-color-base.a-text-normal:contains('adult')").find('a.a-link-normal.a-text-normal').attr('href');
        if(adultLink && adultLink != undefined && adultLink != "undefined" && adultLink != ""){
            adultFilter = false;    
            openLink("https:amazon.com"+adultLink);
            nextPage = adultLink;
            console.log("Removing adult filter, please wait...");
            sleep(5000);
        }
    }
    
    //exits function in case there's notitles to scrapp
    if(dom('#noResultsTitle').length > 0) {
        console.log("Não existe match ente este filtro de Sub-Gênero e Data, tentando o proximo Sub-Gênero...");
        //updates end update of subgenre
        updateLastScrapp('genre',genreGroupsList[needle], 'lastUpdatedEnd');
        needle++;

        //### log ###
        fs.appendFile('CrawlerLog.txt', "\n\n//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#\n"+JSON.stringify(logTotal)+",\n//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#\n", (err) => {
            if (err) throw err;
            console.log('O arquivo total foi salvo!');
            console.log(logTotal);
        });
        //######

        beginScrapp(urlList);
    } else { //in case there's pagination to do, keep on paginating the filter
        //li info (ASIN)
        dom('.s-result-item.celwidget').each(function(i,a) {
            asinList.push(a.attribs['data-asin']);
        });
        
        //a link info (movie data)
        dom('.a-link-normal.s-access-detail-page.s-color-twister-title-link.a-text-normal').each(function(i,a) {
            var existent = false;
            var movieValues = new movieSchema({
                status: 'New',
                name:  a.attribs.title,
                full_url:   a.attribs.href,
                asin: asinList[i],
                genre: genreGroupsList[needle].genreName,
                page: genreGroupsList[needle].url,
                typeOfContent: 'tv'
            }); 

            movieSchema.findOne({asin : asinList[i]}).exec(function(err,movie){
                if(movie != null){
                    existent = movie.asin;
                }
            //});
            
                //### log ###
                var movieInfo = {};
                movieInfo['title'] = a.attribs.title;
                //######

                //save movie info into movie model - - check/update local list of asins
                if(!existent) {
                    movieAsinLocalList.push(asinList[i]);
                    movieValues.save();

                    //### log ###
                    movieInfo['status'] = "saved";
                    logTotal['total_new_movies']++;
                    qtdNew++;
                    //######

                    //console.log("Filme ["+ a.attribs.title+ "] salvo com sucesso! [QTD: "+movieAmount+"]");
                    movieAmount++;
                } else {
                    //### log ###
                    movieInfo['status'] = "exists";
                    logTotal['total_existing_movies']++;
                    qtdExist++;
                    //######

                    //console.log("Filme ["+ a.attribs.title+ "] já existe na base.  [QTD: "+movieAmount+"]");
                }

                //### log ###
                qtdPage++;
                logTotal['total_movies']++;
                logDetail['total_movies_in_page'] = qtdPage;
                logDetail['new_movies_in_page'] = qtdNew;
                logDetail['existing_movies_in_page'] = qtdExist;
                moviesInfo[qtdPage] = movieInfo;
                logDetail['page_movies'] = moviesInfo;
                //######
            });
        });
        
        //update all pages from a single subcategory field (only first page for each page)
        if(dom("#s-result-count").text().substring(0,2) == '1-') {
            textValue = dom(".pagnDisabled").text();
            //console.log('Updated All Pages:'+textValue);

            if(textValue == ""){
                textValue = "< 6";
            }
            //### log ###
            logDetail['filter_total_pages'] = textValue;
            //######

            updateLastScrapp('genre',genreGroupsList[needle], 'allPages', parseInt(textValue));
        }

        //### log ###
        fs.appendFile('CrawlerLog.txt', "\n"+JSON.stringify(logDetail)+",", (err) => {
            if (err) throw err;
            console.log('O arquivo detail foi salvo!');
        });

        logTotal['total_pages']++;
        logDetail['total_movies_in_page'] = "0";
        logDetail['new_movies_in_page'] = "0";
        logDetail['existing_movies_in_page'] = "0";
        logDetail['page_movies'] = [];
        //######

        //check if there's a next page button to scrapp
        if(dom('#pagnNextLink')[0] != undefined && nextPage == false) {
            nextPage = dom("#pagnNextLink")[0].attribs.href;
        } else if (dom('.a-last').text().substr(0,5)=="Sorry" && nextPage == false) { //blocked by robot
            updateLastScrapp('genre',genreGroupsList[needle], 'lastPageUrl', 'Blocked'); 
        }
        callback(nextPage); 
   }
}

//interative internal page scrapp function
function checkNext(page) {
    if(page) {
        
        //updates last page of sub genre (LOGICA DE UPDATE DO LAST PAGE DE CADA SUB-GENERO)
        //lastPage = page.substring(page.indexOf("page=")+5, page.indexOf('&ie'));
        //updateLastScrapp('genre',genreGroupsList[needle], 'lastPage', parseInt(lastPage));
        //updateLastScrapp('genre',genreGroupsList[needle], 'lastPageUrl', page);

        //### log ###
        var actualPg = page.split("&page=").pop().split('&')[0];
        logDetail['page'] = actualPg;
        logDetail['page_url'] = "http://www.amazon.com"+page;
        //######
        
        //check next page
        //c.queue("http://www.amazon.com"+page);
        startCraw("http://www.amazon.com"+page);
    } else {
        //updates end update of subgenre
        updateLastScrapp('genre',genreGroupsList[needle], 'lastUpdatedEnd');
        //grava o update no last_up_end do genre
        console.log("Não existem mais páginas do Sub-Gênero ["+genreGroupsList[needle].genreName+"] para verificar, passando para o próximo Sub-Gênero...");
        //send to next page
        needle++;

        //### log ###
        logDetail['page'] = "1";
        
        fs.appendFile('CrawlerLog.txt', "\n\n//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#\n"+JSON.stringify(logTotal)+",\n//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#\n", (err) => {
            if (err) throw err;
            console.log('O arquivo total foi salvo!');
            console.log(logTotal);
        });
        //######

        beginScrapp(urlList);
    }
}

//iterative page scrapp function
//function beginScrapp(list) {
async function beginScrapp(list) {
    if (typeof list[needle] != 'undefined') {

        //### log ###
        logDetail['genre'] = genreGroupsList[needle].genreName;
        logDetail['subgenre'] = genreGroupsList[needle].subGendName;
        logDetail['decade'] = genreGroupsList[needle].dataRange;
        logDetail['filter_url'] = list[needle];
        logDetail['page_url'] = list[needle];
        logDetail['filter_total_pages'] = "< 6";

        logInfo['genre'] = genreGroupsList[needle].genreName;
        logInfo['subgenre'] = genreGroupsList[needle].subGendName;
        logInfo['decade'] = genreGroupsList[needle].dataRange;
        logInfo['filter_url'] = list[needle];

        fs.appendFile('CrawlerLog.txt', "\n//##########################################################################################\n"+JSON.stringify(logInfo)+",", (err) => {
            if (err) throw err;
            console.log('O arquivo info foi salvo!');
        });
        //######

        //updates begin update of subgenre
        updateLastScrapp('genre',genreGroupsList[needle], 'lastUpdatedBegin');
        //crawl the genre
        //c.queue(list[needle]);
        startCraw(list[needle]);

        //LOGICA DE NAO PASSAR POR SUB-GENEROS CUJA PAGINAÇÃO TENHA SIDO CONCLUÍDA
        /*if(genreGroupsList[needle].allPages != genreGroupsList[needle].lastPage) {
            c.queue(list[needle]);
        } else { //skip to next unfinished genre
            needle++;
            c.queue(list[needle]);
        }*/

    } else {
        console.log("Fim da lista de Canais");

        //### log ###
        fs.appendFile('CrawlerLog.txt', "\n\n#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*\n"+JSON.stringify(logTotal)+",\n#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*\n", (err) => {
            if (err) throw err;
            console.log('O arquivo total foi salvo!');
            console.log(logTotal);
        });
        //######

        await closeBrowser();

        process.exit();

        //kill forever run
        return forever.list(true, function (err, processes) {
            if (processes) {
				console.log('Encerrando o forever');
              	forever.stopAll();
            }
        });
    }
}

//function that updates last date of modify scrapp markers
function updateLastScrapp(modelType, doc, field, value = null) {
    var model;
    //in case of specific value
    if(value == null) {
        value = new Date().toLocaleString();
    }
    if(modelType == 'genre') {
        model = genreGroupSchema;
    } else {
        model = movieSchema;
    }

    //update model
    model.updateOne(
        { _id: doc._id },
        { $set: { [field] : value }
    }, function(err,doc){});
}

//MAIN FUNCTION
//genreGroups.then(function(results) {
genreGroups.then(async function(results) {
    //MAIN FUNCTION
    results.forEach(genreGroup => {
		var hasDate = false;

		if(genreGroup.lastUpdatedEnd){
			//Only update genreGroup after one week
			var dateLastUpdate = new Date(new Date(genreGroup.lastUpdatedEnd).toISOString());
			var dateNow = new Date(new Date (Date.now()).toISOString());
			var diff = dateNow - dateLastUpdate;
			diff = Math.floor(diff / (1000 * 60 * 60 * 12));
			hasDate = true;
		}
        

        if(diff >= 7 || !hasDate){
            genreGroupsList.push(genreGroup);
            urlList.push(genreGroup.url);
        }
    });

    await startBrowser();

    //get all movies asin's
    movies.then(function(res) {
        res.forEach(movie => {
            moviesList.push(movie.asin);
        });

        //### log ###
        fs.appendFile('CrawlerLog.txt', "\n\n//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#\n"+JSON.stringify(logTotal)+",\n//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#\n", (err) => {
            if (err) throw err;
            console.log('O arquivo total foi salvo!');
            console.log(logTotal);
        });
        //######

        //first scrapp
    });
    beginScrapp(urlList);
}); 

function getGenreGroups(){
	//get genergroups
	var genreUrlCollection = [];
	//monta a query para execução
	var query = genreGroupSchema.find({ "subGendercontentType": "tv" }).lean();
	//realiza operacoes com os resultados
	function getGenerGroupFiltered() {
		return new Promise((resolve, reject) => {
			query.exec(function(err,docs) {
				docs.forEach(doc => {
					genreUrlCollection.push(doc);
				});
				//inserir dentro do contexto asyncrono
				resolve(genreUrlCollection);
			});
		});
	}

	//funcao responsavel por gerenciar a promise getgeersgroups
	async function getData() {
		var genresUrl = await getGenerGroupFiltered();
		return genresUrl;
	}

	return getData()
}

function getMovies(){
	//get movies
	var movieCollection = [];
	//monta a query para execução
	var query = movieSchema.find({}).lean();
	//realiza operacoes com os resultados
	function getMovies() {
		return new Promise((resolve, reject) => {
			query.exec(function(err,docs) {
				docs.forEach(doc => {
					movieCollection.push(doc);
				});
				//inserir dentro do contexto asyncrono
				resolve(movieCollection);
			});
		});
	}

	//funcao responsavel por gerenciar a promise getmovies
	async function getData() {
		var moviesList = await getMovies();
		return moviesList;
	}

	return getData();
}