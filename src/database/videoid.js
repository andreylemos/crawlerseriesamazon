const mongoose = require('./connection');

const STATUS = Object.freeze({
	New: 'New',
	InProgress: 'IN PROGRESS',
	Done: 'DONE',
	Error: 'ERROR',
});

const enumToArray = (enumerator) => Object.keys(enumerator).map(key => enumerator[key]); 

const Schema = mongoose.Schema;

const VideoId = new Schema({
	asin: { type: String, index: true, unique: true, required: true },
	name: { type: String, required: true },
	genre: String,
	page: String,
	full_url: String,
	status: { type: String, enum: [ ...enumToArray(STATUS) ], default: STATUS.New },
	updatedStart: Date,
	updatedEnded: Date,
	typeOfContent: String
},
{
	timestamps: { createdAt: true, updatedAt: false },
});

module.exports = mongoose.model('videoId', VideoId);
