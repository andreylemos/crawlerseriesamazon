const mongoose = require('./connection');

const Schema = mongoose.Schema;

const GenreSchema = new Schema({
	name: { type: String, required: true, unique: true },
	url: { type: String, required: true },
	lastUpdated: { type: String },
});

const Genre = mongoose.model('Genre', GenreSchema);

const SubGenreSchema = new Schema({
	genreId: String,
	subGendercontentType: String,
	genreName: String,
	url: String,
	subGenreName: String,
	dataRange: String,
	lastUpdatedBegin: Date,
	lastUpdatedEnd: Date,
	allPages: Number,
	lastPage: Number,
	lastPageUrl: String,
});

const SubGenre = mongoose.model('SubGenre', SubGenreSchema);

module.exports = { Genre, SubGenre };
