const mongoose = require('mongoose');
const movieSchema = require("./schemas/videoId");

mongoose.connect('mongodb://sky:sky123@ds050739.mlab.com:50739/amazon_movies_id', {useNewUrlParser: true});

//get movies
var movieCollection = [];
//monta a query para execução
var query = movieSchema.find({}).lean();
//realiza operacoes com os resultados
function getMovies() {
    return new Promise((resolve, reject) => {
        query.exec(function(err,docs) {
            docs.forEach(doc => {
                movieCollection.push(doc);
            });
            //inserir dentro do contexto asyncrono
            resolve(movieCollection);
        });
    });
}

//funcao responsavel por gerenciar a promise getmovies
async function getData() {
    var moviesList = await getMovies();
    return moviesList;
}

module.exports = getData();

