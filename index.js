const Scraper = require('./src/crawler');
const { Genre, SubGenre } = require('./src/database/genre');
const { to, throwError } = require('./src/utils/helper');
require('./src/database/connection');

async function main() {
	const scraper = new Scraper();
	await scraper.startBrowser();

	//APENAS SERIES
	const [ errorGenres, genres ] = await to(scraper.getGenres("tv"));
	if (errorGenres || !genres) throwError(
		'Houve um erro ao tentar recuperar os gêneros do site da Amazon',
		true,
		errorGenres
	);

	const genresDocs = genres.map((doc) => {
		return {
			updateOne: {
				filter: {
					name: doc.name,
				},
				update: { $set: doc },
				upsert: true,
			},
		};
	});
	
	//salva apenas series
	try {
		const result = await Genre.collection.bulkWrite(genresDocs);
		console.log('Gêneros (TV) salvos com sucesso', result);
	} catch (err) {
		throwError('Houve um erro ao tentar salvar um gênero no banco de dados', true, err);
	}
	
	// ROTINA DE SCRAPP DOS SUB-GENEROS
	// get genres url's
	let genresList = [];
	try {
		genresList = await Genre.find({"name": { "$regex": "TV - ", "$options": "i" }});
	} catch (err) {
		throwError('Houve um erro ao tentar recuperar as urls dos gêneros no banco de dados', true, err);
	}

	let subGenres = [];
	for (const genre of genresList){
		// eslint-disable-next-line max-len
		const [ errorSubGenres, result ] = await to(scraper.getSubGenres(genre.url));
		if (errorSubGenres || !result) throwError(
			'Houve um erro ao tentar recuperar os sub-gêneros do site da Amazon' +
				'\n subGenre: ' +
				result,
			true,
			errorSubGenres
		);
		subGenres.push(result);
	}

	let subGenresDocs = [];
	subGenres.forEach(docs => {
		subGenresDocs = subGenresDocs.concat(docs);
	});

	subGenresDocs = subGenresDocs.map(doc => ({
		updateOne: {
			filter: {
				genreName: doc.genreName,
				subGenreName: doc.subGenreName,
				dataRange: doc.dataRange,
			},
			update: { $set: doc },
			upsert: true,
		},
	}));

	try {
		const result = await SubGenre.collection.bulkWrite(subGenresDocs);
		console.log('Sub Gêneros salvos com sucesso.');

	} catch (err) {
		throwError('Houve um erro ao tentar recuperar as urls dos sub-gêneros no banco de dados', true, err);
	}

	try {
		const result = await SubGenre.find({});
		console.log('Sub Gêneros retornados: ', result.length);

	} catch (err) {
		throwError('Houve um erro ao tentar recuperar as urls dos gêneros no banco de dados', true, err);
	}

	await scraper.closeBrowser();

	require('./src/scrapmovies');
}

main();

//caso alguma promessa quebre
process.on('unhandledRejection', function(err) {
    console.log(err);
    process.exit(1);
});