var Crawler = require("crawler");
const mongoose = require('mongoose');
const Genre  = require("./src/schemas/genre");

//conecta ao banco local (test)
mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});

/*mongoose.connection.on('open', function () {
  mongoose.connection.db.listCollections({name: 'genres'})
      .next(function(err, exists) {
          if (!exists) {
            Genre.create({name: "", url: ""}, function(err, doc) {});
          }
      });
});*/

Genre.collection.drop();

var c = new Crawler({
    maxConnections : 3,
    // This will be called for each crawled page
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;

            let allLists = $(".s-see-all-refinement-list li a").toArray();
            
            //varre cada uma das listas e salva o conteudo de seus links no mongoDB
            allLists.forEach(pageList =>  {
              let genreValues = new Genre({ 
                name: pageList.attribs.title,
                url: pageList.attribs.href
              });
              genreValues.save();
            });
            console.log('Gêneros Coletados.')
        }
        done();
    }
});

async function getGenres() {
  var genres = await c.queue('https://www.amazon.com/gp/search/other/ref=sr_sa_p_n_theme_browse-bin?rh=n%3A2858778011&bbn=2858778011&pickerToList=theme_browse-bin&ie=UTF8&qid=1547555592');
  return genres;
}

module.exports = getGenres();